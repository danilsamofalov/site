<?php

namespace App\Http\Controllers\Main;

use App\Category;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BuiltController extends Controller
{
    public function index()
    {
        $built=News::select('content')->where('id', 3)->get();
        return view('main.built_in_messenger', compact('built'));
    }
}
