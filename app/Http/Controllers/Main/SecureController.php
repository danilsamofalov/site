<?php

namespace App\Http\Controllers\Main;

use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SecureController extends Controller
{
    public function index()
    {
        $secure=News::select('content')->where('id', 1)->get();
        return view('main.secure_and_reliable', compact('secure'));
    }
}
