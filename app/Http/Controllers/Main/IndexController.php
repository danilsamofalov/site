<?php

namespace App\Http\Controllers\Main;

use App\Category;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function __invoke()
    {
        $plants=Category::select('category')->where('id', 4)->get();
        return view('main.index', compact('plants'));
    }
}
