<?php

namespace App\Http\Controllers\Main;

use App\Category;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LiveController extends Controller
{
    public function index()
    {
        $live=News::select('content')->where('id', 2)->get();
        return view('main.live_video', compact('live'));
    }
}
