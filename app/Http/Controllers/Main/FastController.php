<?php

namespace App\Http\Controllers\Main;

use App\Category;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FastController extends Controller
{
    public function index()
    {
        $fast=News::select('content')->where('id', 4)->get();
        return view('main.fast_instantly', compact('fast'));
    }
}
