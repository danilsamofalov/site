<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public function news()
    {
        return $this->belongsToMany(Category::class, 'category_news', 'news_id', 'category_id');
    }
}
