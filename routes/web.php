<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'main'], function () {
   \Illuminate\Support\Facades\Route::get('/','IndexController');
});
\Illuminate\Support\Facades\Route::get('/built', 'Main\BuiltController@index')->name('built.index');
\Illuminate\Support\Facades\Route::get('/fast', 'Main\FastController@index')->name('fast.index');
\Illuminate\Support\Facades\Route::get('/live', 'Main\LiveController@index')->name('live.index');
\Illuminate\Support\Facades\Route::get('/secure', 'Main\SecureController@index')->name('secure.index');


Auth::routes();



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
