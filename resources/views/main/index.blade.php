@extends('layouts.main')

@section('content')
    <main>
        <section class="edica-landing-section edica-landing-about">
            <div class="container">
                <div class="row">
                    <div class="col-md-6" data-aos="fade-up-right">
                        <h4 class="edica-landing-section-subtitle-alt">ABOUT US</h4>
                        <h2 class="edica-landing-section-title">1000+ customer using Our application.</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipng elit, sed do eiusmod tempor incididunt ut
                            labore aliqua. Ut enim que minim veniam, quis nostrud exercitation.</p>
                        <ul class="landing-about-list">
                            <li>Powerful and flexible theme</li>
                            <li>Simple, transparent pricing</li>
                            <li>Build tools and full documention</li>
                        </ul>
                    </div>
                    <div class="col-md-6" data-aos="fade-up-left">
                        <img src="{{asset('assets/images/phone-copy.png')}}" width="468px" alt="about"
                             class="img-fluid">
                    </div>
                </div>
            </div>
        </section>
        <section class="edica-landing-section edica-landing-clients">
            <div class="container">
                <h4 class="edica-landing-section-subtitle-alt">PARTNER WITH US</h4>
                <div class="partners-wrapper">
                    <img src="{{asset('assets/images/Partner_1.png')}}" alt="client logo" data-aos="flip-right"
                         data-aos-delay="250">
                    <img src="{{asset('assets/images/Partner_2.png')}}" alt="client logo" data-aos="flip-right"
                         data-aos-delay="500">
                    <img src="{{asset('assets/images/Partner_3.png')}}" alt="client logo" data-aos="flip-right"
                         data-aos-delay="750">
                    <img src="{{asset('assets/images/Partner_4.png')}}" alt="client logo" data-aos="flip-right"
                         data-aos-delay="1000">
                    <img src="{{asset('assets/images/Partner_5.png')}}" alt="client logo" data-aos="flip-right"
                         data-aos-delay="1250">
                    <img src="{{asset('assets/images/Partner_6.png')}}" alt="client logo" data-aos="flip-right"
                         data-aos-delay="1500">
                </div>
            </div>
        </section>
        <section class="edica-landing-section edica-landing-services">
            <div class="container">
                <h4 class="edica-landing-section-subtitle">Service We Offer</h4>
                <h2 class="edica-landing-section-title">What features you will <br> Get from App.</h2>
                <div class="row">
                    <div class="col-md-6 landing-service-card" data-aos="fade-right">
                        <img src="{{asset('assets/images/picture.svg')}}" alt="card img"
                             class="img-fluid service-card-img">
                        <h4 class="service-card-title">Live Video</h4>
                        <p class="service-card-description">He has led a remarkable campaign, defying the traditional
                            mainstream parties courtesy of his En Marche! movement. For many, however, the.</p>
                    </div>
                    <div class="col-md-6 landing-service-card" data-aos="fade-left">
                        <img src="{{asset('assets/images/internet.svg')}}" alt="card img"
                             class="img-fluid service-card-img">
                        <h4 class="service-card-title">Secure and Reliable</h4>
                        <p class="service-card-description">He has led a remarkable campaign, defying the traditional
                            mainstream parties courtesy of his En Marche! movement. For many, however, the.</p>
                    </div>
                    <div class="col-md-6 landing-service-card" data-aos="fade-right">
                        <img src="{{asset('assets/images/goal.svg')}}" alt="card img"
                             class="img-fluid service-card-img">
                        <h4 class="service-card-title">Fast. Instantly.</h4>
                        <p class="service-card-description">He has led a remarkable campaign, defying the traditional
                            mainstream parties courtesy of his En Marche! movement. For many, however, the.</p>
                    </div>
                    <div class="col-md-6 landing-service-card" data-aos="fade-left">
                        <img src="{{asset('assets/images/chat-bubble.svg')}}" alt="card img"
                             class="img-fluid service-card-img">
                        <h4 class="service-card-title">Built-in Messenger</h4>
                        <p class="service-card-description">He has led a remarkable campaign, defying the traditional
                            mainstream parties courtesy of his En Marche! movement. For many, however, the.</p>
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
