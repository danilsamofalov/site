@extends('layouts.main')

@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edica :: Home</title>
    <link rel="stylesheet" href="assets/vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="assets/vendors/font-awesome/css/all.min.css">
    <link rel="stylesheet" href="assets/vendors/aos/aos.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="assets/vendors/jquery/jquery.min.js"></script>
    <script src="assets/js/loader.js"></script>
</head>
<body>
<main>
    <div class="container">
        <div class="row">
            <div class="col-lg-11 mx-auto">
                @foreach($live as $live)
                <h1 class="edica-page-title" data-aos="fade-up">{{$live->content}}</h1>
                @endforeach
                <section class="edica-contact py-5 mb-5">
                    <div class="row">
                        <div class="col-md-4 contact-sidebar" data-aos="fade-left">
                            <div class="embed-responsive embed-responsive-1by1 contact-map">
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>
</body>

</html>
@endsection
